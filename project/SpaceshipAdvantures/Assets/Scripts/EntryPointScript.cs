﻿using UnityEngine;
using System.Collections;

public class EntryPointScript : MonoBehaviour {

    private FPSCounter fpsCounter;

    // GUI skin
    private GUISkin main_scene_skin;

    // label
    public GUIStyle debugLabel;

	// Use this for initialization
	void Start () 
    {
        fpsCounter = this.GetComponent<FPSCounter>();

        //
        main_scene_skin = Resources.Load("MainGUISkin") as GUISkin;
	}
	
    void OnGUI()
    {
        GUI.skin = main_scene_skin;

        // Color in GUI skin
        debugLabel.normal.textColor = Color.red;

        GUILayout.BeginVertical();
            GUILayout.Label("FPS: " + fpsCounter.getFPS().ToString(), debugLabel);
        GUILayout.EndVertical();
    
    }

	// Update is called once per frame
	void Update () {
	
	}
}
