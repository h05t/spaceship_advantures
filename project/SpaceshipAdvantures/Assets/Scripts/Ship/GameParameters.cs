﻿using UnityEngine;
using System.Collections;

public class GameParameters
{
    private static GameParameters gameParameters = new GameParameters();
    private GameParameters()
    {
    }

    public static GameParameters GetInstance()
    {
        return gameParameters;
    }

    // Переделать в проценты!!!!!!!!!!!!!!!!!!!
    public int fuel = 199;
    public int O2 = 199;
    public int health = 199;
    public bool isDead = false;
}