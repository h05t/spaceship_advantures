﻿using UnityEngine;
using System.Collections;

public class O2BarManager : MonoBehaviour
{
    public float timeBetweenLostO2 = 1f;
    public int lostO2Amound = 5;
    public int lostHealthWithoutO2Amound = 30;
    float timer; 

    GameObject O2BarGO;
    IndicatorBar O2Bar;

    GameObject healthBarGO;
    IndicatorBar healthBar;
   
    void Awake()
    {
        O2BarGO = GameObject.FindGameObjectWithTag("O2Bar");
        O2Bar = O2BarGO.GetComponent<IndicatorBar>();

        healthBarGO = GameObject.FindGameObjectWithTag("healthBar");
        healthBar = healthBarGO.GetComponent<IndicatorBar>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Если персонаж жив, то он дишит и может умиреть:
        if (false == GameParameters.GetInstance().isDead)
        {
            // Add the time since Update was last called to the timer.
            timer += Time.deltaTime;

            // If the timer exceeds the time between attacks, the player is in range and this enemy is alive...
            if (timer >= timeBetweenLostO2 && GameParameters.GetInstance().O2 > 0)
            {
                // Сначала изменяем значение в глобальных данных:
                GameParameters.GetInstance().O2 = GameParameters.GetInstance().O2 - lostO2Amound;

                // Исменяем представлени данных:
                O2Bar.O2Width = GameParameters.GetInstance().O2;
                timer = 0f;
            }

            // Если у игрока не осталось кислорода то он начинает умирать:
            if (GameParameters.GetInstance().O2 <= 0 && GameParameters.GetInstance().health > 0 && timer >= timeBetweenLostO2)
            {
                // Сначала изменяем значение в глобальных данных:
                GameParameters.GetInstance().health = GameParameters.GetInstance().health - lostHealthWithoutO2Amound;

                // Исменяем представлени данных:
                healthBar.O2Width = GameParameters.GetInstance().health;
                timer = 0f;
            }
        }

        // Если у игрока не осталось хелсов то он умер:
        if (GameParameters.GetInstance().health <= 0)
        {
            // Сначала изменяем значение в глобальных данных:
            GameParameters.GetInstance().isDead = true;
        }
    }
}
