﻿using UnityEngine;
using System.Collections;

/*

 * This code assumes that all your planets are tagged 'Planet'. This code goes on the ship or projectile. 
 * This is sample code. For real code, I'd create an array of transforms from the array of game objects, 
 * and I'd cache the local transform to make things a bit more efficient. 'maxGravDist' defines the maximum 
 * distance from the planet the gravity is applied, and maxGravity is the maximum amount of force applied each frame. 
 * The equation makes the gravity drop off linearly. Real gravity drops off as the square of the distance, but usually 
 * a linear dropoff works well in games.
 
*/

public class PlanetGravity : MonoBehaviour 
{
    public float maxGravDist = 4.0f;
    public float maxGravity = 35.0f;
     
    GameObject[] planets;
     
    void Start () 
    {
        planets = GameObject.FindGameObjectsWithTag("Planet");
    }
     
    void FixedUpdate () 
    {
        foreach(GameObject planet in planets) 
        {
            float dist = Vector3.Distance(planet.transform.position, transform.position);
            if (dist <= maxGravDist) 
            {
                Vector3 v = planet.transform.position - transform.position;
                rigidbody2D.AddForce(v.normalized * (1.0f - dist / maxGravDist) * maxGravity);
            }
        }
    }
}