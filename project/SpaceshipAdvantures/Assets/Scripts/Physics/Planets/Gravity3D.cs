﻿using UnityEngine;
using System.Collections;

public class Gravity3D : MonoBehaviour
{
    public float force_amount = 10f;

    void OnTriggerStay(Collider col)
    {
        //  Рабочая версия гравитации для 3d пространства:
        col.rigidbody.AddForce((transform.position - col.transform.position).normalized * force_amount);//, ForceMode.Force);

        //  Версия гравитации для 2d пространства:
        //Vector3 tmp3dVec = (transform.position - col.transform.position).normalized * force_amount;
        //col.rigidbody2D.AddForce(new Vector2(tmp3dVec.x, tmp3dVec.y));//, ForceMode.Force);
    }
}